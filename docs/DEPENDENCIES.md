# Dependency copyrights and licenses

## Vitepress

Copyright (c) 2019-present, Yuxi (Evan) You

[MIT](https://github.com/vuejs/vitepress/blob/v1.2.3/LICENSE)
